\select@language {croatian}
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Umjetna neuronska mre\IeC {\v z}a}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Umjetni neuron}{4}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Prijenosna funkcija}{5}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Neuronske mre\IeC {\v z}e kao univerzalni aproksimatori}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}Regresija i klasifikacija}{10}{section.2.3}
\contentsline {section}{\numberline {2.4}Konvolucijske neuronske mre\IeC {\v z}e}{10}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Konvolucijski sloj}{11}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Sloj sa\IeC {\v z}imanja}{12}{subsection.2.4.2}
\contentsline {chapter}{\numberline {3}U\IeC {\v c}enje neuronske mre\IeC {\v z}e}{14}{chapter.3}
\contentsline {section}{\numberline {3.1}Gradijentni spust}{15}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Stohasti\IeC {\v c}ki gradijentni spust}{16}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Metode ubrzavanja u\IeC {\v c}enja}{16}{subsection.3.1.2}
\contentsline {subsubsection}{Metoda zaleta}{16}{section*.9}
\contentsline {subsubsection}{Druge metode}{17}{section*.10}
\contentsline {section}{\numberline {3.2}Propagacija pogre\IeC {\v s}ke unatrag}{17}{section.3.2}
\contentsline {section}{\numberline {3.3}Pobolj\IeC {\v s}avanje u\IeC {\v c}enja i generalizacije}{20}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Podjela skupa podataka}{20}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Pronala\IeC {\v z}enje dobre arhitekture mre\IeC {\v z}e}{21}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Regularizacija}{21}{subsection.3.3.3}
\contentsline {subsubsection}{$L^1$ i $L^2$ regularizacija}{21}{section*.13}
\contentsline {subsubsection}{Isklju\IeC {\v c}ivanje neurona}{22}{section*.14}
\contentsline {subsubsection}{Umjetno pro\IeC {\v s}irivanje skupa za u\IeC {\v c}enje}{22}{section*.15}
\contentsline {chapter}{\numberline {4}Semanti\IeC {\v c}ka segmentacija}{24}{chapter.4}
\contentsline {section}{\numberline {4.1}Skupovi podataka za semanti\IeC {\v c}ku segmentaciju}{25}{section.4.1}
\contentsline {chapter}{\numberline {5}Programska izvedba}{27}{chapter.5}
\contentsline {section}{\numberline {5.1}Kori\IeC {\v s}teni alati i tehnologije}{27}{section.5.1}
\contentsline {section}{\numberline {5.2}Struktura programske izvedbe}{28}{section.5.2}
\contentsline {section}{\numberline {5.3}Priprema i pristup podacima}{28}{section.5.3}
\contentsline {section}{\numberline {5.4}Preprocesiranje}{29}{section.5.4}
\contentsline {section}{\numberline {5.5}Arhitektura mre\IeC {\v z}e}{30}{section.5.5}
\contentsline {section}{\numberline {5.6}Postprocesiranje}{31}{section.5.6}
\contentsline {chapter}{\numberline {6}Rezultati}{33}{chapter.6}
\contentsline {section}{\numberline {6.1}Mjere}{33}{section.6.1}
\contentsline {section}{\numberline {6.2}Rezultati na skupu \emph {Stanford Background Dataset}}{34}{section.6.2}
\contentsline {chapter}{\numberline {7}Zaklju\IeC {\v c}ak}{37}{chapter.7}
\contentsline {chapter}{Literatura}{38}{chapter*.25}
