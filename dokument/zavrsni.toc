\select@language {croatian}
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Umjetna neuronska mre\IeC {\v z}a}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Umjetni neuron}{4}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Prijenosna funkcija}{5}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Neuronske mre\IeC {\v z}e kao univerzalni aproksimatori}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}Regresija i klasifikacija}{10}{section.2.3}
\contentsline {section}{\numberline {2.4}Konvolucijske neuronske mre\IeC {\v z}e}{10}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Konvolucijski sloj}{11}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Sloj sa\IeC {\v z}imanja}{12}{subsection.2.4.2}
\contentsline {chapter}{\numberline {3}U\IeC {\v c}enje neuronske mre\IeC {\v z}e}{14}{chapter.3}
\contentsline {section}{\numberline {3.1}Gradijentni spust}{15}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Stohasti\IeC {\v c}ki gradijentni spust}{16}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Postupci ubrzavanja u\IeC {\v c}enja}{16}{subsection.3.1.2}
\contentsline {subsubsection}{Dodavanje inercije}{16}{section*.9}
\contentsline {subsubsection}{Drugi postupcii algoritmi}{17}{section*.10}
\contentsline {subsubsection}{Algoritam \emph {Adam}}{17}{section*.11}
\contentsline {section}{\numberline {3.2}Propagacija pogre\IeC {\v s}ke unatrag}{18}{section.3.2}
\contentsline {section}{\numberline {3.3}Pobolj\IeC {\v s}avanje u\IeC {\v c}enja i generalizacije}{21}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Podjela skupa podataka}{21}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Pronala\IeC {\v z}enje dobre arhitekture mre\IeC {\v z}e}{22}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Regularizacija}{22}{subsection.3.3.3}
\contentsline {subsubsection}{$L^1$-regularizacija i $L^2$-regularizacija}{22}{section*.14}
\contentsline {subsubsection}{Isklju\IeC {\v c}ivanje neurona}{23}{section*.15}
\contentsline {subsubsection}{Umjetno pro\IeC {\v s}irivanje skupa za u\IeC {\v c}enje}{23}{section*.16}
\contentsline {chapter}{\numberline {4}Semanti\IeC {\v c}ka segmentacija}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}Skupovi podataka za semanti\IeC {\v c}ku segmentaciju}{26}{section.4.1}
\contentsline {chapter}{\numberline {5}Programska izvedba}{28}{chapter.5}
\contentsline {section}{\numberline {5.1}Kori\IeC {\v s}teni alati i tehnologije}{28}{section.5.1}
\contentsline {section}{\numberline {5.2}Struktura programske izvedbe}{29}{section.5.2}
\contentsline {section}{\numberline {5.3}Priprema i pristup podacima}{30}{section.5.3}
\contentsline {section}{\numberline {5.4}Preprocesiranje}{31}{section.5.4}
\contentsline {section}{\numberline {5.5}Arhitektura mre\IeC {\v z}e}{32}{section.5.5}
\contentsline {section}{\numberline {5.6}Postprocesiranje}{33}{section.5.6}
\contentsline {chapter}{\numberline {6}Rezultati}{35}{chapter.6}
\contentsline {section}{\numberline {6.1}Mjere}{35}{section.6.1}
\contentsline {section}{\numberline {6.2}Rezultati na skupu \emph {Stanford Background Dataset}}{36}{section.6.2}
\contentsline {chapter}{\numberline {7}Zaklju\IeC {\v c}ak}{39}{chapter.7}
\contentsline {chapter}{Literatura}{40}{chapter*.30}
