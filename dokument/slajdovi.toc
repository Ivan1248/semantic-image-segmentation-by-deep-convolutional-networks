\beamer@endinputifotherversion {3.36pt}
\select@language {croatian}
\beamer@sectionintoc {1}{Uvod}{3}{0}{1}
\beamer@sectionintoc {2}{Umjetne neuronske mre\IeC {\v z}e}{5}{0}{2}
\beamer@sectionintoc {3}{U\IeC {\v c}enje neuronske mre\IeC {\v z}e}{9}{0}{3}
\beamer@sectionintoc {4}{Semanti\IeC {\v c}ka segmentacija}{13}{0}{4}
\beamer@sectionintoc {5}{Izvedba sustava za semanti\IeC {\v c}ku segmentaciju}{15}{0}{5}
\beamer@sectionintoc {6}{Rezultati}{21}{0}{6}
\beamer@sectionintoc {7}{Kraj}{28}{0}{7}
