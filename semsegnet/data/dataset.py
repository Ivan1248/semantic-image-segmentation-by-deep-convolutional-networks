from typing import Tuple, List
import numpy as np
import random
from .abstract_dataset import AbstractDataset
from data.dataset_dir import load_images, load_labels, load_info


class Dataset(AbstractDataset):
    def __init__(self, images: List[np.ndarray], labels: List[np.ndarray], class_count: int):
        super().__init__(images, labels, class_count)

    def shuffle(self, order_determining_number: float = -1):
        """ Shuffles the data. """
        image_label_pairs = list(zip(self.images, self.labelings))
        if order_determining_number < 0:
            random.shuffle(image_label_pairs)
        else:
            random.shuffle(image_label_pairs, lambda: order_determining_number)
        self.images[:], self.labelings[:] = zip(*image_label_pairs)

    def split(self, start, end):
        """ Splits the dataset into two smaller datasets. """
        first = Dataset(self.images[start:end], self.labelings[start:end], self.class_count)
        second = Dataset(self.images[:start] + self.images[end:], self.labelings[:start] + self.labelings[end:],
                         self.class_count)
        return first, second

    @staticmethod
    def load(dataset_directory: str):
        images = load_images(dataset_directory)
        labels = load_labels(dataset_directory)
        class_count = load_info(dataset_directory).classes_count
        return Dataset(images, labels, class_count)
