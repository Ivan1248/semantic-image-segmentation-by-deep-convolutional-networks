import os.path
import cv2
import numpy as np
from util import path, directory
from data.dataset_dir import save_image, save_labeling, save_info
import data.dataset_dir
from data.preparers import AbstractPreparer
from processing import adjust_shape


class UNSPreparer(AbstractPreparer):
    SHAPE = (240, 320)

    @staticmethod
    def prepare(data_path: str, shape=SHAPE):
        out_data_path = data_path + '.prepared'
        if os.path.exists(out_data_path):
            print("Dataset already prepared. Delete " + out_data_path + " if you would like it to be prepared again.")
            return out_data_path
        os.makedirs(out_data_path)

        in_path = os.path.join(data_path, 'train')
        out_images_path = data.dataset_dir.get_images_dir(out_data_path)
        out_labels_path = data.dataset_dir.get_labels_dir(out_data_path)
        os.makedirs(out_images_path)
        os.makedirs(out_labels_path)

        names = [path.get_file_name_without_extension(p) for p in
                 directory.get_files(in_path) if not p.endswith('_mask.tif')]

        for name in names:
            in_image_path = os.path.join(in_path, name + '.tif')
            image = cv2.imread(in_image_path)
            save_image(image, out_images_path, name)

            in_labeling_path = os.path.join(in_path, name + '_mask.tif')
            labeling = cv2.imread(in_labeling_path) // 255
            save_labeling(labeling, out_labels_path, name)

        save_info(out_data_path, class_count=2)

        return out_data_path
