from typing import Tuple
from abc import *
import random

class AbstractDataset:
    def __init__(self, images: list, labels: list, class_count: int):
        self._images = images
        self._labelings = labels
        self._class_count = class_count
        #self._index_in_epoch = 0

    def __len__(self):
        return len(self.images)

    def __getitem__(self, key):
        if isinstance(key, slice):
            return self._images[key.start:key.stop:key.step], self.labelings[key.start:key.stop:key.step]
        else:  # int
            return self._images[key], self.labelings[key]

    @property
    def size(self) -> int:
        return len(self)

    @property
    def image_shape(self) -> Tuple[int, int, int]:
        return self.images[0].shape

    @property
    def class_count(self):
        return self._class_count

    @abstractmethod
    def shuffle(self, order_determining_number: float = -1):
        pass

    @abstractmethod
    def split(self, start, end) -> tuple:
        pass

    @property
    def images(self):
        return self._images

    @property
    def labelings(self):
        return self._labelings

    def get_an_example(self):
        i = random.randint(0, self.size - 1)
        return self.images[i], self.labelings[i]
