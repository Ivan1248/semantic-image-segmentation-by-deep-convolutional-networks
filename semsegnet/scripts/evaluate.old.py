from semsegnet import SemSegNetB, Optimizers
from data.dataset import Dataset
import data
from processing.labels import dense_to_one_hot, one_hot_to_dense
import evaluation
import processing

model_paths = [
    '../storage/models_nazgul/rmsprop1-0.001-3stg-3lay-12deep,f1/70epochs-69acc',
    '../storage/models_nazgul/rmsprop1-0.001-3stg-3lay-12deep,f2/60epochs-61acc',
    '../storage/models_nazgul/rmsprop1-0.001-3stg-3lay-12deep,f3/70epochs-73acc',
    '../storage/models_nazgul/rmsprop1-0.001-3stg-3lay-12deep,f4/70epochs-80acc',
    '../storage/models_nazgul/rmsprop1-0.001-3stg-3lay-12deep,f5/70epochs-76acc']
# model_path = '../storage/models/rmsprop-0.002-3stg-3lay-12deep,f1/40epochs-67acc'
data_path = '../storage/datasets/iccv09Data'
# models_path = '../storage/models'

models_path = '../storage/models'
model_path = '../storage/models_nazgul/rmsprop1-0.001-3stg-3lay-12deep,f3/70epochs-73acc'

data = Dataset.load(data.preparers.Iccv09Preparer.prepare(data_path))
for f in range(2, 5):
    mp = model_paths[f]
    print(mp)
    ssn = SemSegNetB(data, first_layer_depth=12, batch_size=1, optimizer=Optimizers.rmsprop1em3,
                     test_part=(f * 0.2, f * 0.2 + 0.2), name='f' + str(f + 1))


    images = ssn.test_data.images
    labels = ssn.test_data.labelings
    ssn.load_state(mp)
    predictions = ssn.test2()
    predictions = ssn._label_segments(images, [processing.one_hot_to_dense(p) for p in predictions])
    p, r, i, x = evaluation.compute_precision_recall_iou_pa(
        [dense_to_one_hot(p - 1, ssn.test_data.class_count, 0) for p in predictions],
        [dense_to_one_hot(l, ssn.test_data.class_count, 1) for l in labels])
    del ssn

"""
1 ?
Precision: 0.609395863661
Recall (class accuracy): 0.616007991931
IOU accuracy: 0.463310550726
"""
"""
2 ?
Precision: 0.460376375794
Recall (class accuracy): 0.399633042556
IOU accuracy: 0.333553455957
"""
"""
3
Precision: 0.465395677791
Recall (class accuracy): 0.386685027155
IOU accuracy: 0.317150309611
Pixel accuracy: 0.728628077652
"""
