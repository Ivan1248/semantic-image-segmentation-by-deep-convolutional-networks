#import parent
from model.semsegnet import SemSegNetB, Optimizers
from data.dataset import Dataset
import data
from processing.labels import dense_to_one_hot, one_hot_to_dense
import evaluation
import processing
import random
import numpy as np
import cv2
from util.display_window import DisplayWindow
import util.directory as directory

model_paths = [
    '../storage/models/adam--3stg-3lay-12deep,adam2dropout78093,f5/160epochs-83acc',
    '../storage/models_nazgul/rmsprop1-0.001-3stg-3lay-12deep,f2/60epochs-61acc',
    '../storage/models_nazgul/rmsprop1-0.001-3stg-3lay-12deep,f3/70epochs-73acc',
    '../storage/models_nazgul/rmsprop1-0.001-3stg-3lay-12deep,f4/70epochs-80acc',
    '../storage/models_nazgul/rmsprop1-0.001-3stg-3lay-12deep,f5/70epochs-76acc']
# model_path = '../storage/models/rmsprop-0.002-3stg-3lay-12deep,f1/40epochs-67acc'
data_path = '../storage/datasets/iccv09Data'
# models_path = '../storage/models'

#models_path = '../storage/models'
#model_path = '../storage/models_nazgul/rmsprop1-0.001-3stg-3lay-12deep,f3/70epochs-73acc'

# random.seed(78093)
print("Preparing (if necessary) and loading data...")
dataset = Dataset.load(data.preparers.Iccv09Preparer.prepare(data_path))
# dataset.shuffle(random.random())

fold_count = 5
epoch_count = 1

mp = model_paths[0]
print(mp)
files = directory.get_files('../storage/images')

images = [cv2.imread(f) for f in files if f.endswith('.png')]

ssn = SemSegNetB(test_data=dataset, train_data=dataset,
                 batch_size=11, first_layer_depth=12, optimizer=Optimizers.adam)
ssn.load_state(mp)
labelings = ssn.run(images, no_postprocess=True)
w = DisplayWindow('results')
labelings = ssn._label_segments(images, labelings)
for im, lab in zip(images, labelings):
    lab = processing.denormalize(lab * (1 / dataset.class_count))
    key = w.display_with_labels(im, lab)
    if key == 27:
        break
del w
del ssn

"""
1 ?
Precision: 0.609395863661
Recall (class accuracy): 0.616007991931
IOU accuracy: 0.463310550726
"""
"""
2 ?
Precision: 0.460376375794
Recall (class accuracy): 0.399633042556
IOU accuracy: 0.333553455957
"""
"""
3
Precision: 0.465395677791
Recall (class accuracy): 0.386685027155
IOU accuracy: 0.317150309611
Pixel accuracy: 0.728628077652
"""
