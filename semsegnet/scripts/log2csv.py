import re
import parent
import util.file as file

log_path = '../storage/models/adam--3stg-3lay-12deep,adam278093,f4/160epochs-83acc.log'

jmbag_ime = dict()
jmbag_lv_bodovi = dict()
lv_max = 0

test_cost_csv = ["epoch, cost", "0,0.37"]
test_accuracy_csv = ["epoch, accuracy", "0,0.12"]

train_cost_csv = ["epoch, cost", "0,0.37"]
train_accuracy_csv = ["epoch, accuracy", "0,0.12"]

train_d_cost_csv = ["epoch, cost", "0,0.37"]

with open(log_path, encoding='utf-8') as fs:
    lines = fs.readlines()
    epoch = 1
    train_d_cost_sum = 0
    train_d_cost_n = 0
    i = 0
    while i < len(lines):
        l = lines[i]
        i += 1
        # if len(l) == 0 or l.startswith('Train') or l.startswith('Testing'):
        #    continue
        if l.startswith('Test:'):
            _, _, cost, _, accuracy = l.split(' ')[:5]
            cost = float(cost[:-1])
            accuracy = float(accuracy[:-1])
            test_cost_csv.append('{:d},{:3f}'.format(epoch, cost))
            test_accuracy_csv.append('{:d},{:3f}'.format(epoch, accuracy))

            train_d_cost_csv.append('{:d},{:3f}'.format(epoch, train_d_cost_sum / train_d_cost_n))
            train_d_cost_sum, train_d_cost_n = 0, 0
            epoch += 1
            continue
        elif l.startswith('Train:'):
            _, _, cost, _, accuracy = l.split(' ')[:5]
            cost = float(cost[:-1])
            accuracy = float(accuracy[:-1])
            train_cost_csv.append('{:d},{:3f}'.format(epoch - 1, cost))
            train_accuracy_csv.append('{:d},{:3f}'.format(epoch, accuracy))
            continue
        elif bool(re.search('\d\d:.*', l)):
            _, _, _, _, _, _, cost, _, accuracy = l.split(' ')[:9]
            cost = float(cost[:-1])
            accuracy = float(accuracy[:-1])
            train_d_cost_sum += cost
            train_d_cost_n += 1

print('\n')
print('Test cost:')
for l in test_cost_csv:
    print(l)

print('\n')
print('Training cost:')
for l in train_cost_csv:
    print(l)

print('\n')
print('Test accuracy:')
for l in test_accuracy_csv:
    print(l)

print('\n')
print('Training accuracy:')
for l in train_accuracy_csv:
    print(l)

"""print('\n')
print('Training-dropout:')
for l in train_d_cost_csv:
    print(l)"""
