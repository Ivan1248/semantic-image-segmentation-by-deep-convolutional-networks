import parent
from model import preprocessing
from processing.format import normalize, denormalize, to_laplacian_pyramid, yuv_to_rgb, rgb_to_yuv
import cv2
import numpy as np
import os
from util.display_window import DisplayWindow

image = cv2.imread('../storage/images/fer-1.png')
# pyr = preprocessing.normalized_yuv_laplacian_pyramid(image, 3)
pyr = to_laplacian_pyramid(normalize(rgb_to_yuv(image)), 3 + 1)[:3]
win = DisplayWindow()
if not os.path.exists('pyr'):
    os.makedirs('pyr')
for i in range(3):
    for c in range(3):
        stage = pyr[i][:, :, c]
        stage += 1  # -stage.min()
        stage *= 127
        stage = stage.astype(np.uint8)
        print(str(np.max(stage)))
        print(str(np.min(stage)))
        win.display(stage)
        stage = cv2.cvtColor(stage, cv2.COLOR_GRAY2RGB)
        cv2.imwrite('./pyr/pyr-{}{}.png'.format(i, ('y', 'u', 'v')[c]), stage)
