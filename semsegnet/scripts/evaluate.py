import parent
from semsegnet import SemSegNetB, Optimizers
from data.dataset import Dataset
import data
from processing.labels import dense_to_one_hot, one_hot_to_dense
import evaluation
import processing
import random
import numpy as np
import os
import processing.shape
import processing.transform
import postprocessing

rmsprop_nonshuffled = {'dir': '../storage/models_nazgul/rmsprop1-0.001-3stg-3lay-12deep,f*/',
                       'epochs': 70,
                       'optimizer': Optimizers.rmsprop2em3}

adam = {'dir': '../storage/models/adam--3stg-3lay-12deep,adam2dropout78093,f*//',
        'epochs': 160,
        'optimizer': Optimizers.adam}

adamnd = {'dir': '../storage/models/adam--3stg-3lay-12deep,adam2nondropout78093,f*//',
          'epochs': 160,
          'optimizer': Optimizers.adam}


def evaluate(model=adam):
    model_paths = []
    for i in range(5):
        path = model['dir'].replace('*', str(i + 1))
        for f in os.listdir(path):
            file = os.path.join(path, f)
            if os.path.isfile(os.path.join(path, f)) and str(model['epochs']) + 'epochs' in f and '.' not in f:
                model_paths.append(file)
                break

    # model_path = '../storage/models/rmsprop-0.002-3stg-3lay-12deep,f1/40epochs-67acc'
    data_path = '../storage/datasets/iccv09Data'
    # models_path = '../storage/models'

    seed = 78093
    random.seed(78093)
    print("Preparing (if necessary) and loading data...")
    dataset = Dataset.load(data.preparers.Iccv09Preparer.prepare(data_path))
    dataset.shuffle(random.random())

    fold_count = 5
    epoch_count = 1

    pa, ra, fa, xa = 0, 0, 0, 0

    for f in range(0, fold_count):
        mp = model_paths[f]
        print(mp)
        test_part = (np.array([f, f + 1]) / fold_count * dataset.size + 0.5).astype(int)
        test_data, train_data = dataset.split(*test_part)
        ssn = SemSegNetB(test_data=test_data, train_data=test_data,
                         batch_size=11, first_layer_depth=12, optimizer=model['optimizer'],
                         name='test,f' + str(f + 1))
        images = test_data.images
        labels = test_data.labelings

        def remove_paddings():
            shape = images[0].shape
            shape = (shape[0], shape[1])

            def find_x_padding(labeling):
                for m in range(labeling.shape[0]):
                    if labeling[m, 0] != 0:
                        return m * 2
                return 0

            def find_y_padding(labeling):
                for m in range(labeling.shape[1]):
                    if labeling[0, m] != 0:
                        return m * 2
                return 0

            for i in range(len(images)):
                m = find_y_padding(labels[i])
                if m != 0:
                    n1 = shape[1] - m
                    n0 = int(shape[0] * n1 / shape[1])
                    s = (n0, n1)
                    images[i] = processing.shape.crop(images[i], s)
                    images[i] = processing.shape.resize(images[i], (shape[1], shape[0]))
                    labels[i] = processing.shape.crop(labels[i], s)
                    labels[i] = processing.shape.resize(labels[i], (shape[1], shape[0]))
                    continue
                m = find_x_padding(labels[i])
                if m != 0:
                    n0 = shape[0] - m
                    n1 = int(shape[1] * n0 / shape[0])
                    s = (n0, n1)
                    images[i] = processing.shape.crop(images[i], s)
                    images[i] = processing.shape.resize(images[i], (shape[1], shape[0]))
                    labels[i] = processing.shape.crop(labels[i], s)
                    labels[i] = processing.shape.resize(labels[i], (shape[1], shape[0]))

        # remove_paddings()

        ssn.load_state(mp)
        predictions = []
        ssn.test(test_data, lambda pred: predictions.append(processing.one_hot_to_dense(pred)))
        predictions = [postprocessing.label_segments(images[i], predictions[i]) for i in range(len(images))]
        p, r, f, x = evaluation.compute_precision_recall_iou_pa(
            [dense_to_one_hot(p - 1, ssn.test_data.class_count, 0) for p in predictions],
            [dense_to_one_hot(l, ssn.test_data.class_count, 1) for l in labels])
        pa += p
        ra += r
        fa += f
        xa += x
        del ssn
    pa /= fold_count
    ra /= fold_count
    fa /= fold_count
    xa /= fold_count

    print('average accuracy: precision {}, recall {}, iou {}, pixel {}'.format(pa, ra, fa, xa))


if __name__ == '__main__':
    print("ADAM")
    evaluate(adam)

"""
1 ?
Precision: 0.609395863661
Recall (class accuracy): 0.616007991931
IOU accuracy: 0.463310550726
"""
"""
2 ?
Precision: 0.460376375794
Recall (class accuracy): 0.399633042556
IOU accuracy: 0.333553455957
"""
"""
3
Precision: 0.465395677791
Recall (class accuracy): 0.386685027155
IOU accuracy: 0.317150309611
Pixel accuracy: 0.728628077652
"""

"""
adam
1
Precision: 0.666982129243
Recall (class accuracy): 0.634629640874
IOU accuracy: 0.504817218144
ixel accuracy: 0.741384943182

adam+SLIC
1
Precision: 0.666982129243
Recall (class accuracy): 0.634629640874
IOU accuracy: 0.504817218144
ixel accuracy: 0.741384943182

2
Precision: 0.673950809624
Recall (class accuracy): 0.723059334245
IOU accuracy: 0.572523421535
ixel accuracy: 0.761711010344

3
Precision: 0.600633825541
Recall (class accuracy): 0.51555635804
IOU accuracy: 0.428475685889
ixel accuracy: 0.822073954691

4
Precision: 0.561055349405
Recall (class accuracy): 0.463998217909
IOU accuracy: 0.400319170292
ixel accuracy: 0.828305197407

5
Precision: 0.688883528767
Recall (class accuracy): 0.674587966513
IOU accuracy: 0.559283040784
ixel accuracy: 0.795310496795

avg
Recall (class accuracy): 0.6023
Pixel accuracy: 0,7897
"""
