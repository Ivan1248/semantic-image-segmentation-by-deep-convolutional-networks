from semsegnet import SemSegNetB, Optimizers
from data.dataset import Dataset
import data
import random
import numpy as np


def train(data_path, models_path):
    seed = 78093
    random.seed(78093)
    print("Preparing (if necessary) and loading data...")
    dataset = Dataset.load(data.preparers.UNSPreparer.prepare(data_path))
    dataset.shuffle(random.random())

    fold_count = 5
    epoch_count = 160
    save_interval = 20

    for f in range(0, fold_count):
        test_part = (np.array([f, f + 1]) / fold_count * dataset.size + 0.5).astype(int)
        test_data, train_data = dataset.split(*test_part)
        ssn = SemSegNetB(test_data=test_data, train_data=train_data,
                         batch_size=11, first_layer_depth=12, optimizer=Optimizers.adam,  # rmsprop2em3
                        save_path=models_path, name='adam22dropout{},f{}'.format(seed, f + 1))
        #model_path = '../storage/models/adam--3stg-3lay-12deep,adam2dropout78093,f5/160epochs-83acc'
        #ssn.load_state(model_path)
        for e in range(epoch_count // save_interval):
            ssn.train(epochs=save_interval, train_accuracy_log_period=5, keep_prob=0.5)
            ssn.save_state()
        ssn.test()
        del ssn


if __name__ == "__main__":
    data_path = '../storage/datasets/UNSData'
    models_path = '../storage/models'
    train(data_path, models_path)
