#log_path = '../storage/models_nazgul/rmsprop1-0.001-3stg-3lay-12deep,f5/70epochs-76acc.log'
log_path = '../storage/models/adam--3stg-3lay-12deep,adamtest78093,f1/160epochs-76acc.log'

import os
import re

jmbag_ime = dict()
jmbag_lv_bodovi = dict()
lv_max = 0

test_cost_csv = ["epoch, cost"]
train_cost_csv = ["epoch, cost"]

with open(log_path, encoding='utf-8') as fs:
    lines = fs.readlines()
    epoch = 1
    train_cost_sum = 0
    train_cost_n = 0
    i = 0
    while i < len(lines):
        l = lines[i]
        i += 1
        if len(l) == 0 or l.startswith('Train') or l.startswith('Testing'):
            continue
        elif l.startswith('Test:'):
            _, _, cost, _, accuracy = l.split(' ')[:5]
            cost = float(cost[:-1])
            accuracy = float(accuracy[:-1])
            test_cost_csv.append('{:d},{:3f}'.format(epoch, cost))
            train_cost_csv.append('{:d},{:3f}'.format(epoch, train_cost_sum / train_cost_n))
            train_cost_sum, train_cost_n = 0, 0
            epoch += 1
            continue
        elif bool(re.search('\d\d:.*', l)):
            _, _, _, _, _, _, cost, _, accuracy = l.split(' ')[:9]
            cost = float(cost[:-1])
            accuracy = float(accuracy[:-1])
            train_cost_sum += cost
            train_cost_n += 1

print('Testing:')
for l in test_cost_csv:
    print(l)

print('\n')
print('Training:')
for l in train_cost_csv:
    print(l)

