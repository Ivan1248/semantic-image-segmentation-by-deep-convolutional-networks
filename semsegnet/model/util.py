import numpy as np


def class_accuracy(trues: list, predictions: list, class_count: int):
    results = np.zeros((class_count, 2))
    for l in range(len(trues)):
        # t = denso
        for i in range(trues[l].shape[0]):
            for j in range(trues[l].shape[1]):
                t = trues[l][i, j]
                results[t, t == predictions[l][i, j]] += 1
    return results
