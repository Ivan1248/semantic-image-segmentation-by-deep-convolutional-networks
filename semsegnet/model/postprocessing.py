import numpy as np
from processing import segmentation


def label_segments(image, labeling):
    segments = segmentation(image, segments_count=400, compactness=4, sigma=2)
    out = np.zeros(image.shape[:2], dtype="uint8")
    for (i, seg_val) in enumerate(np.unique(segments)):
        mask = segments == seg_val
        out[mask] = np.argmax(np.bincount(labeling[mask]))
    return out
