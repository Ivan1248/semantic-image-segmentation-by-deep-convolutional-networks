import tensorflow as tf


def weight_variable(size: int, in_channels: int, out_channels: int):
    return tf.Variable(
        tf.truncated_normal([size, size, in_channels, out_channels], stddev=0.1, mean=0))


def bias_variable(n: int):
    return tf.Variable(tf.constant(0.1, shape=[n]))


def conv2d(x, w):
    return tf.nn.conv2d(input=x, filter=w, strides=[1, 1, 1, 1], padding='SAME')


def max_pool(x, dim: int):
    return tf.nn.max_pool(x, ksize=[1, dim, dim, 1], strides=[1, dim, dim, 1], padding='SAME')


def upscale(x, factor):
    shape = x.get_shape()[1:3]
    if factor == 1:
        return x
    return tf.image.resize_nearest_neighbor(x, [int(shape[i]) * factor for i in range(2)])


def softmax2d(input: tf.Tensor):
    shape = input.get_shape()
    s = [-1] + [int(shape[i]) for i in range(1, 4)]
    return tf.reshape(tf.nn.softmax(tf.reshape(input, [-1, 9])), s)


"""def normalize(self, x, train=True):
    if train:
        mean, variance = tf.nn.moments(x, [0, 1, 2])
        assign_mean = self.mean.assign(mean)
        assign_variance = self.variance.assign(variance)
        with tf.control_dependencies([assign_mean, assign_variance]):
            return tf.nn.batch_norm_with_global_normalization(
                x, mean, variance, self.beta, self.gamma,
                self.epsilon, self.scale_after_norm)
    else:
        mean = self.ewma_trainer.average(self.mean)
        variance = self.ewma_trainer.average(self.variance)
        local_beta = tf.identity(self.beta)
        local_gamma = tf.identity(self.gamma)
        return tf.nn.batch_norm_with_global_normalization(
            x, mean, variance, local_beta, local_gamma,
            self.epsilon, self.scale_after_norm)"""

