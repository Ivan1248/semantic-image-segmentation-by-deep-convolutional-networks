import tensorflow as tf


class Optimizers:
    gd1em1 = ["gd", tf.train.GradientDescentOptimizer, 1e-1]
    rmsprop2em3 = ["rmsprop", tf.train.RMSPropOptimizer, 2e-3]
    rmsprop1em3 = ["rmsprop", tf.train.RMSPropOptimizer, 1e-3]
    adagrad5em1 = ["adagrad", tf.train.AdagradOptimizer, 5e-1]
    adam = ['adam', tf.train.AdamOptimizer]
    adam2 = ['adam', tf.train.AdamOptimizer, 2e-3]
