import numpy as np


def dense_to_one_hot(labels_dense, class_count):
    """
        Converts each integer pixel from {0..C} to a one-hot vector
        with dimension C, where C = `class_count`
        WxH -> WxHxC 
    """
    return (np.arange(class_count) == (labels_dense[:, :, None])).astype(np.uint8)


def one_hot_to_dense(labels_one_hot):
    """ Converts pixel labels from one-hot vectors to intgers. """
    return np.argmax(labels_one_hot, axis=2)
