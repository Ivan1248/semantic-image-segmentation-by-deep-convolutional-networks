from skimage.segmentation import slic, find_boundaries, mark_boundaries, felzenszwalb, quickshift
import skimage.segmentation as seg
import cv2
import numpy as np


def segmentation(image, segments_count=300, compactness=4, sigma=1):
    # http://scikit-image.org/docs/dev/auto_examples/plot_segmentations.html
    # return seg.quickshift(image, kernel_size=3, max_dist=6, ratio=0.5)
    #return seg.felzenszwalb(image, scale=400, sigma=0.5, min_size=20)
    return seg.slic(image, n_segments=segments_count, compactness=compactness, sigma=sigma)

"""
if __name__ == '__main__':
    def test(input):
        s = segmentation(input, 200)
        s = seg.mark_boundaries(input, s, color=(0, 0, 0))
        return s

    import os
    import processing
    from util.display_window import DisplayWindow

    print(os.getcwd())
    b = np.load('_test/test.label.npy')
    from processing.labels import dense_to_one_hot

    a = cv2.imread('_test/3000945.png')
    b = label_segments(a, cv2.cvtColor(a, cv2.COLOR_RGB2GRAY))
    lab = processing.denormalize(b)
    w = DisplayWindow()
    with w:
        w.display_with_labels(a, lab)
    # c = mark_boundaries(a, b)
    cv2.imshow('a', b)
    cv2.waitKey(8000)
    cv2.imshow('a', test(a))
    cv2.waitKey(8000)
    pass
"""
