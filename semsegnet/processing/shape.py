import cv2
import numpy as np


def crop(image: np.ndarray, maxshape):
    d = [max(0, image.shape[i] - maxshape[i]) for i in [0, 1]]
    return image[(d[0] + 1) // 2:image.shape[0] - (d[0] // 2), (d[1] + 1) // 2:image.shape[1] - (d[1] // 2)]


def pad(image: np.ndarray, shape):
    d = [shape[i] - image.shape[i] for i in [0, 1]]
    return cv2.copyMakeBorder(image, (d[0] + 1) // 2, d[0] // 2, (d[1] + 1) // 2, d[1] // 2, cv2.BORDER_CONSTANT, 0)


def adjust_shape(image: np.ndarray, shape):
    return pad(crop(image, shape), shape)


def resize(image: np.ndarray, shape):
    return cv2.resize(image, shape,interpolation=cv2.INTER_NEAREST)

