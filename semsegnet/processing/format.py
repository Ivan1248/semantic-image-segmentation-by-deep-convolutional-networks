import cv2
from typing import List
import numpy as np


def rgb_to_yuv(image: np.ndarray) -> np.ndarray:
    return cv2.cvtColor(image, cv2.COLOR_RGB2YUV)


def yuv_to_rgb(image: np.ndarray) -> np.ndarray:
    return cv2.cvtColor(image, cv2.COLOR_YUV2RGB)


def normalize(image: np.ndarray) -> np.ndarray:
    """ Converts values from [0, 255] to [0.0, 1.0]. """
    return image.astype(np.float32) * (1.0 / 255)


def denormalize(image: np.ndarray) -> np.ndarray:
    """ Converts values from [0.0, 1.0] to [0. 255]. """
    return (image * 255).astype(np.uint8)


def to_laplacian_pyramid(image: np.ndarray, n: int) -> List[np.ndarray]:
    last = image
    layers = []
    for i in range(n - 1):
        g = cv2.pyrDown(last)
        layers.append(cv2.subtract(last, cv2.pyrUp(g)))
        last = g
    layers.append(last)
    return layers


def from_laplacian_pyramid(pyr: List[np.ndarray]) -> np.ndarray:
    if len(pyr) == 1:
        return pyr[0]
    return cv2.add(pyr[0], cv2.pyrUp(from_laplacian_pyramid(pyr[1:])))


def normalize_locally(image: np.ndarray, block_size):
    # TODO
    return image.copy()
